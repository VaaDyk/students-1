﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class StudentGroup {
    private String GroupName;
    private int StudentsQuantity;
    private int MaxOfStudents;
    private Student[] Students;

    public StudentGroup() {}

    public StudentGroup(String GroupName, int StudentsQuantity) {
        Student[] temp = new Student[StudentsQuantity];

        this.GroupName = GroupName;
        this.StudentsQuantity = 0;
        this.MaxOfStudents = StudentsQuantity;
        this.Students = temp;
    }

    public void Push_back(Student newStudent) {
        int newCurrent = this.StudentsQuantity + 1;

        if ( newCurrent > this.MaxOfStudents ) {
            Student[] newArray = new Student[newCurrent];

            if (newArray == null ) {
                System.Console.WriteLine("You want to much.");
            }

            ArrayCopy(newArray, this.Students, this.StudentsQuantity);
            this.Students = newArray;
        }

        this.Students[this.StudentsQuantity] = newStudent;
        this.Students[this.StudentsQuantity].SetGroup(this);
        this.StudentsQuantity = newCurrent;
    }

    public void Delete(int index) {
        int limit = this.StudentsQuantity - 1;

        for (int i = index + 1; i < limit; i++) {
            this.Students[i] = this.Students[i + 1];
        }

        this.StudentsQuantity = limit;
    }

    public int GetCourse() {
        String temp = this.GroupName[3].ToString();
        int firstNumber = Convert.ToInt32(temp);
        int thisYear = 2018 % 1000 % 10;
        int flag = Convert.ToInt32(DateTime.Today.Month >= 9);

        return thisYear + flag;
    }

    public override String ToString() {
        String temp = this.GroupName + ":\n";

        for ( int counter = 0; counter < this.StudentsQuantity; counter++ ) {
            temp += Students[counter].GetSecondName() + " " + Students[counter].GetFirstName() + " " + Students[counter].GetLastName() + ", ";
            temp += "karma: " + Students[counter].GetKarma() + ", scholarship: " + Students[counter].GetScholarship() + "\n";
        }

        return temp;
    }

    void ArrayCopy(Student[] Target, Student[] Sourse, int quantity) {
        for (int i = 0; i < quantity; i++) {
            Target[i].SetSecondName(Sourse[i].GetSecondName());
            Target[i].SetFirstName(Sourse[i].GetSecondName());
            Target[i].SetLastName(Sourse[i].GetLastName());
            Target[i].SetScholarship(Sourse[i].GetScholarship());
            Target[i].SetKarma(Sourse[i].GetKarma());
        }
    }
}
